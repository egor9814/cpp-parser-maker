#!/usr/bin/env bash

_app="${BASH_SOURCE[0]}"
while [ -h "$_app" ]; do
  _dir="$( cd -P "$( dirname "$_app" )" > /dev/null 2>&1 && pwd )"
  _app="$(readlink "$_app")"
  [[ $_app != /* ]] && _app="$DIR/$_app"
done
_dir="$( cd -P "$( dirname "$_app" )" > /dev/null 2>&1 && pwd )"

_lib="$_dir/cpp-threading-library"
if [ ! -d "$_lib" ]; then
  echo "cpp-threading-library not found!"
else
  _build="$_lib/.build"
  if [ -d "$_build" ]; then
    rm -rf "$_build"
  fi
  mkdir -p "$_build" && cd "$_build"
  if [ -f "../CMakeLists.txt" ]; then
    cmake ..
    make threading
    cd "$_dir"
    cp -vf "$_build/src/libthreading.so" "$_dir/"
    echo "function (link_threading target)" > "$_dir/cpp-threading-library.cmake"
    echo "    target_link_libraries(\${target} \"$_dir/libthreading.so\")" >> "$_dir/cpp-threading-library.cmake"
    echo "endfunction(link_threading)" >> "$_dir/cpp-threading-library.cmake"
    echo "done!"
  else
    echo "CMakeLists.txt not found!"
  fi
fi
