#ifndef __egor9814__fsm_character_hpp__private__
#define __egor9814__fsm_character_hpp__private__

struct Character {
    enum class Type : uint8_t {
        Epsilon,
        Normal
    };

    bool isNormal() const {
        return type == Type::Normal;
    }

    bool isEpsilon() const {
        return type == Type::Epsilon;
    }

    static const Character& Epsilon() {
        static Character epsilon;
        return epsilon;
    }

private:
    wchar_t chr {0};
    Type type {Type::Epsilon};

public:
    Character() = default;
    explicit Character(wchar_t value) : type(Type::Normal), chr(value) {}

    Character(const Character& c) : chr(c.chr), type(c.type) {}
    Character(Character&& c) noexcept {
        Character temp(c);
        swap(temp);
    }

    Character&operator=(const Character& c) {
        if (this != &c) {
            this->chr = c.chr;
            this->type = c.type;
        }
        return *this;
    }

    Character&operator=(Character&& c) noexcept {
        if (this != &c) {
            Character temp(c);
            swap(temp);
        }
        return *this;
    }

    void swap(Character& c) {
        std::swap(c.chr, this->chr);
        std::swap(c.type, this->type);
    }

    Type getType() const {
        return type;
    }

    wchar_t getValue() const {
        return chr;
    }
};

inline bool operator<(const Character& a, const Character& b) {
    if (a.isNormal() && b.isNormal())
        return a.getValue() < b.getValue();
    return a.getType() < b.getType();
}

#endif //__egor9814__fsm_character_hpp__private__
