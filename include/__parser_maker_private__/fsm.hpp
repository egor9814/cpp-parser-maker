#ifndef __egor9814__fsm_hpp__private__
#define __egor9814__fsm_hpp__private__

#include "fsm_character.hpp"

#include "fsm_state.hpp"


struct __IDCounter__ {
    state_id_t next() {
        return id++;
    }

private:
    state_id_t id {0};
};
#define IDCounter __IDCounter__& id

#include "fsm_nfa.hpp"

#endif //__egor9814__fsm_hpp__private__
