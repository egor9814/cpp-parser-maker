#ifndef __egor9814__fsm_nfa_hpp__private__
#define __egor9814__fsm_nfa_hpp__private__

class NFA;
using NFAPtr = std::shared_ptr<NFA>;

using NFATable = std::map<token_t, NFAPtr>;

class NFA {
    StatePtr _begin, _final;

    NFA(StatePtr begin, StatePtr final) : _begin(std::move(begin)), _final(std::move(final)) {}
    static NFAPtr newInstance(StatePtr begin, StatePtr final) {
        return NFAPtr(new NFA(begin, final));
    }

public:
    NFA(const NFA& nfa) : _begin(nfa._begin), _final(nfa._final) {}

    NFA(NFA&& nfa) noexcept {
        NFA temp(nfa);
        swap(temp);
    }

    NFA&operator=(const NFA& nfa) {
        if (this != &nfa) {
            _begin = nfa._begin;
            _final = nfa._final;
        }
        return *this;
    }

    NFA&operator=(NFA&& nfa) noexcept {
        if (this != &nfa) {
            NFA temp(nfa);
            swap(temp);
        }
        return *this;
    }

    void swap(NFA& other) {
        _begin.swap(other._begin);
        _final.swap(other._final);
    }

    const StatePtr& begin() const {
        return _begin;
    }

    const StatePtr& final() const {
        return _final;
    }


    /**
     * Make NFA of char: (A) -- char --> (B)
     */
    static NFAPtr ofChar(wchar_t chr, IDCounter) {
        auto begin = State::newInstance(id.next());
        auto final = State::newInstance(id.next(), true);
        begin->transition(chr, final);
        return newInstance(begin, final);
    }

    /**
     * Make NFA of union:
     * (A) -- ε --> a -- ε --> (B)
     *    \-- ε --> b -- ε -->/
     */
    static NFAPtr ofUnion(const NFAPtr& a, const NFAPtr& b, IDCounter) {
        auto begin = State::newInstance(id.next());
        auto final = State::newInstance(id.next(), true);

        begin->transition(Character::Epsilon(), a->begin());
        begin->transition(Character::Epsilon(), b->begin());

        a->final()->resetAccept().
        transition(Character::Epsilon(), final);

        b->final()->resetAccept().
        transition(Character::Epsilon(), final);

        return newInstance(begin, final);
    }

    /**
     * Make NFA of concat:
     * a -- ε --> b
     */
    static NFAPtr ofConcat(const NFAPtr& a, const NFAPtr& b) {
         a->final()->resetAccept().
         transition(Character::Epsilon(), b->begin());
         return newInstance(a->begin(), b->final());
    }

    /**
     * Make NFA of Kleene Closure (zero-or-more):
     * A = nfa.begin
     * B = nfa.final
     * (C) -- ε --> A ........ B -- ε --> (D)
     *   \           \<-- ε --/          /
     *   \------------------------ ε -->/
     */
    static NFAPtr ofKleeneClosure(const NFAPtr& nfa, IDCounter) {
        auto begin = State::newInstance(id.next());
        auto final = State::newInstance(id.next(), true);

        begin->transition(Character::Epsilon(), final);
        begin->transition(Character::Epsilon(), nfa->begin());

        nfa->final()->resetAccept().
        transition(Character::Epsilon(), final).
        transition(Character::Epsilon(), nfa->begin());

        return newInstance(begin, final);
    }

    /**
     * Make NFA of one-or-more:
     * A = nfa.begin
     * B = nfa.final
     * (C) -- ε --> A ........ B -- ε --> (D)
     *               \<-- ε --/
     */
    static NFAPtr ofOneOrMore(const NFAPtr& nfa, IDCounter) {
        auto begin = State::newInstance(id.next());
        auto final = State::newInstance(id.next(), true);

        begin->transition(Character::Epsilon(), nfa->begin());
        nfa->final()->resetAccept().
        transition(Character::Epsilon(), final).
        transition(Character::Epsilon(), nfa->begin());

        return newInstance(begin, final);
    }

    /**
     * Make NFA of Optional (zero-or-one):
     * A = nfa.begin
     * B = nfa.final
     * (C) -- ε --> A ........ B -- ε --> (D)
     *   \                               /
     *   \------------------------ ε -->/
     */
    static NFAPtr ofZeroOrOne(const NFAPtr& nfa, IDCounter) {
        auto begin = State::newInstance(id.next());
        auto final = State::newInstance(id.next(), true);

        begin->transition(Character::Epsilon(), final);
        begin->transition(Character::Epsilon(), nfa->begin());

        nfa->final()->resetAccept().
        transition(Character::Epsilon(), final);

        return newInstance(begin, final);
    }

    /**
     * Make final NFA
     * @param nfas Table of pair (Token name, NFA)
     */
    static NFAPtr makeFinal(const NFATable& nfaTable, IDCounter) {
        auto begin = State::newInstance(id.next());
        auto final = State::newInstance(id.next(), true);

        for (auto& it : nfaTable) {
            begin->transition(Character::Epsilon(), it.second->begin());
            it.second->final()->
            transition(Character::Epsilon(), final).
            addToken(it.first);
        }

        return newInstance(begin, final);
    }
};

#endif //__egor9814__fsm_nfa_hpp__private__
