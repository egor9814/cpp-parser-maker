#ifndef __egor9814__fsm_state_hpp__private__
#define __egor9814__fsm_state_hpp__private__

struct State;
using StatePtr = std::shared_ptr<State>;
using StateSet = std::set<StatePtr>;
using TransitionMap = std::map<Character, StateSet>;

using state_id_t = uint32_t;
using token_t = std::wstring;
using TokenList = std::vector<token_t>;

class State {
    state_id_t id;
    bool accept;

private:
    //using CharacterPtr = std::shared_ptr<Character>;
    TransitionMap outputStates {}, inputStates {};
    TokenList tokens {};

    StatePtr self {this, [](void*){}};

    explicit State(state_id_t id, bool accept) : id(id), accept(accept) {}
public:
    static StatePtr newInstance(state_id_t id, bool accept = false) {
        return StatePtr(new State(id, accept));
    }

    State(const State& s)
        : id(s.id),
          accept(s.accept),
          outputStates(s.outputStates), inputStates(s.inputStates),
          tokens(s.tokens) {}

    State(State&& s) noexcept {
        State temp(s);
        swap(temp);
    }

    State&operator=(const State& s) {
        if (this != &s) {
            id = s.id;
            accept = s.accept;
            outputStates = s.outputStates;
            inputStates = s.inputStates;
            tokens = s.tokens;
        }
        return *this;
    }

    State&operator=(State&& s) noexcept {
        if (this != &s) {
            State temp(s);
            swap(temp);
        }
        return *this;
    }

    void swap(State& s) {
        std::swap(s.id, id);
        std::swap(s.accept, accept);
        std::swap(s.outputStates, outputStates);
        std::swap(s.inputStates, inputStates);
        std::swap(s.tokens, tokens);
    }

    state_id_t getId() const {
        return id;
    }

    bool isAccept() const {
        return accept;
    }

    State& setAccept() {
        accept = true;
        return *this;
    }

    State& resetAccept() {
        accept = false;
        return *this;
    }

    State& transition(const Character& c, const StatePtr& state) {
        outputStates[c].insert(state);
        state->inputStates[c].insert(self);
        return *this;
    }

    State& transition(wchar_t chr, const StatePtr& state) {
        transition(Character(chr), state);
        return *this;
    }

    State& transition(const Character& c, const StateSet& states) {
        for (const auto& it : states) {
            transition(c, it);
        }
        return *this;
    }

    StateSet getTransitions(const Character& c) const {
        StateSet res;
        auto copy(outputStates);
        for (auto& it : copy[c]) {
            res.insert(it);
        }
        return std::move(res);
    }

    StateSet getTransitions(wchar_t chr) const {
        return getTransitions(Character(chr));
    }

    TransitionMap getAllTransitions() const {
        TransitionMap res;
        for (auto& it : outputStates) {
            auto states = res[it.first];
            for (auto& s : it.second) {
                states.insert(s);
            }
        }
        return std::move(res);
    }

    void removeTransition(const Character& c, const StatePtr& state) {
        outputStates[c].erase(state);
    }

    TransitionMap getAllInputStates() const {
        TransitionMap res;
        for (auto& it : inputStates) {
            auto states = res[it.first];
            for (auto& s : it.second) {
                states.insert(s);
            }
        }
        return std::move(res);
    }

    void removeInputState(const Character& c, const StatePtr& state) {
        inputStates[c].erase(state);
    }

    void addTokens(const TokenList& tokens) {
        for (auto& it : tokens) {
            this->tokens.push_back(it);
        }
    }

    void addToken(const token_t token) {
        tokens.push_back(token);
    }

    const TokenList& getTokens() const {
        return tokens;
    }

    std::wstring toString() {
        std::wstringstream ss;
        if (accept)
            ss << '(';
        ss << '<' << id << '>';
        if (accept)
            ss << ')';
        return ss.str();
    }
};

inline bool operator<(const State& a, const State& b) {
    if (a.getId() < b.getId())
        return true;
    return static_cast<uint8_t>(a.isAccept()) < static_cast<uint8_t>(b.isAccept());
}

inline bool operator<(const StatePtr& a, const StatePtr& b) {
    return *a < *b;
}

#endif //__egor9814__fsm_state_hpp__private__
